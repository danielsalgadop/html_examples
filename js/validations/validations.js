function limpiarCaracteres(string){
	return string.replace(/[^ a-zA-Z0-9]/g,""); // cleans all alphanumeric but spaces
}

function cleanNonDigits(string){
	return string.replace(/\D/g,"");
}

///////////////////////////////////
// Function: validacionesTelefonos
// uses esTelefonoOK
// Receives:
//	- array of telephones
//	- returns 2 arrays clean_telephones, dirty_telephones
// Uses function esTelefonoOK
///////////////////////////////////
function validacionesTelefonos(telephones){
	var clean_telephones=[];
	var dirty_telephones=[];
	for (telephone of telephones){
		if(esTelefonoOK(telephone)){
			clean_telephones.push(telephone);
		}
		else{
			dirty_telephones.push(telephone);
		}
	}
	return [clean_telephones,dirty_telephones];
}

function esTelefonoOK(test_telephone){
	var result_match = test_telephone.match(/^\d{5,20}$/);
	var val_return;
	val_return = (result_match) ? true : false;
	return val_return;
}



function trimSpaces(string){
	// Bulti in .trim exist!!!!    OJO IE9.0+
	// return string.trim();
	
	// Option 1 - not working
	// return string.replace(/[^\s*|\s*$]/g,"");
	// Option 2 - not working
	// string = string.replace(/[^\s*]/g,"");
	// string = string.replace(/[\s*$]/g,"");
	// return string
	// Option 3 - Working but too long;
	var regex_i = /^\s/;
	var regex_d = /\s$/;
	var obj_regex_i = new RegExp(regex_i);
	var obj_regex_d = new RegExp(regex_d);
	var sucio_i = 1; // significa que HAY espacios en blanco
	var sucio_d = 1;
	while (sucio_i == 1 || sucio_d == 1){
		if (obj_regex_i.test(string)){ // si es asi, existe espacio en blanco en la izquierda
			string = string.replace(/^\s/,"")
		}
		else{
			sucio_i = 0;
		}
		if (obj_regex_d.test(string)){ // si es asi, existe espacio en blanco en la izquierda
			string = string.replace(/\s$/,"")
		}
		else{
			sucio_d = 0;
		}
	}
	return(string);	
}

function TieneCaracterRaro(string,excepciones){
	var regex=/[a-zA-Z0-9]/; // Si quieres poner caracteres raros y que la funcion la clasifique como valido(devuelva caracter vacio), ponlo en las excepciones
	var obj_regex = new RegExp(regex);
	
	var regex_ex;  //regular expresion, excepciones
	var obj_regex_ex;
	split_partida=string.split("");
	var novalen="";
	var pie_error="\n------------------------\nCaracteres permitidos= ALFANUMERICOS "; //  mensaje con los caracters "excepcion";
	if ((excepciones == 0) || (!excepciones)){ // si la excepcion es 0 o NO se ha declarado excepcion, solo permite [a-zA-Z0-9]
		obj_regex_ex = obj_regex; // el objeto con la expresion regular es igual (realmente voy a comprobar lo mismo abajo 2 veces.... se puede mejorar esto con un flag, por ejemplo)
	}
	else if (excepciones == 1){
		// permito los espcaios y barras bajas
		regex_ex = /( |_|\\|\.|\-|\(|\))/;
		obj_regex_ex = new RegExp(regex_ex);
		pie_error += ", ESPACIO EN BLANCO, _ , \ , . , -  y ()";
	}
	else if (excepciones == 2){ // Para la descripcion de Mantenimiento > Equipos (descripcion), muy parecido al 1 pero con @
		regex_ex = /(_|\.|\@|\\|-|\/|\(|\)|\ )/;
		obj_regex_ex = new RegExp(regex_ex);
		pie_error += ", ESPACIO EN BLANCO, _ , . , \ , _ , @ , - , ()"; 

	}
	for(var a=0;a<split_partida.length;a++){
		if(!obj_regex.test(split_partida[a])){ // si NO matcheas es que eres caracter RARO
			if (!obj_regex_ex.test(split_partida[a])){  // aplico la excepcion
				novalen+="\nCaracter no valido="+split_partida[a]+"\n";
			}
		}
	}
	if (novalen != ""){
			novalen+=pie_error;
	}
	return(novalen);
}


function esIP(ip){
	return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip);

	// var result_match = ip.match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);
	// var val_return;
	// val_return = (result_match) ? true : false;
	// return val_return; 
}


function esHost(host){
	// alert("recibido "+host);
	if(host){  // avoid testing if received undefined
		// alert("voy a retornar "+/^[a-zA-Z0-9]{1,}$/.test(host));
		if(/^[a-zA-Z]+/.test(host)){ // starts with a letter
			return /^[a-zA-Z0-9_-]{1,}$/.test(host);
		}
	}
	return false;
}