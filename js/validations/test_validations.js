// QUnit.test( "hello test", function( assert ) {
// assert.ok( 1 == 1, "Passed!" );
// });
QUnit.test( "limpiarCaracteres", function( assert ) {
	assert.ok( limpiarCaracteres("eo") == "eo", "If no strange parameters are passed, return the same function received" );
	assert.ok( limpiarCaracteres("e&·$%%&&/(()%o") == "eo", "Lots of strange characters ·$%%&&/(()" );
});
QUnit.test( "trimSpaces", function( assert ) {
	assert.ok( trimSpaces("") 				== "", "Empty returns empty");
	assert.ok( trimSpaces("         ") 		== "", "Full of only spaces returns emtpy");
	assert.ok( trimSpaces("   a      ") 	== "a", "Lots of triming to do");
});
QUnit.test( "cleanNonDigits", function( assert ) {
	assert.ok( cleanNonDigits("") 				== "", "Empty returns empty");
	assert.ok( cleanNonDigits("123") 			== "123", "digits returns same digits");
	assert.ok( cleanNonDigits("12DWGW3") 		== "123", "Letters are removed");
	assert.ok( cleanNonDigits("12·$%%&/3&&&") 	== "123", "Strange characters are removed");
});
QUnit.test( "esTelefonoOK", function( assert ) {
	assert.ok(esTelefonoOK("") 			=== false,"Empty telephone returns false");
	assert.ok(esTelefonoOK("123") 		=== false,"Small telephone returns false");
	assert.ok(esTelefonoOK("12345689112345678921234567893") 	=== false,"telephone 30 digits returns false");
	assert.ok(esTelefonoOK("12345") 	=== true,"telephone 5 digits returns true");
}); 

	// r_validacionesTelefonos = validacionesTelefonos(["asdfas","sdfasdf"]);
	// alert(r_validacionesTelefonos[0]);
QUnit.test( "validacionesTelefonos", function( assert ) {
	assert.deepEqual( validacionesTelefonos(["12345","12345","12345"]),[["12345","12345","12345"],[]], "all clean telephones");
	assert.deepEqual( validacionesTelefonos(["12345","12345","1234"]),[["12345","12345"],["1234"]], "1 dirty telephone");

	// assert.deepEqual(validacionesTelefonos(["111","222","333"]),[r_validacionesTelefonos[0],r_validacionesTelefonos[0]],"Returning 2 arrays");
});

QUnit.test( "esIP", function( assert ) {
	assert.ok( esIP("0.0.0.0") === true, "0.0.0.0 valid IP");
	assert.ok( esIP("255.255.255.255") === true, "255.255.255.255 valid IP");
	assert.ok( esIP("123.123.123.123") === true, "123.123.123.123 valid IP");
	assert.ok( esIP("255.255.255.256") === false, "255.255.255.256 invalid IP");
	assert.ok( esIP("123. 123.123.123") === false, "(with spaces) invalid IP");
	assert.ok( esIP(" 123.123.123.123") === false, "(with spaces) invalid IP");
	assert.ok( esIP("123.123.123.123 ") === false, "(with spaces) invalid IP");
	assert.ok( esIP("") === false, "empty invalid IP");
	assert.ok( esIP() === false, "without parameters invalid IP");
});

QUnit.test( "esHost", function( assert ) {
	assert.ok( esHost("HOST1") === true, "HOST1 valid HOST");
	assert.ok( esHost("HOST-1") === true, "HOST-1 valid HOST");
	assert.ok( esHost("HOST_1") === true, "HOST_1 valid HOST");
	assert.ok( esHost("ADF04TESSDM32") === true, "ADF04TESSDM32 valid HOST");
	assert.ok( esHost("H") === true, "H valid HOST");
	assert.ok( esHost("H_1-23-sadf_sdf") === true, "H_1-23-sadf_sdf valid HOST");
	assert.ok( esHost("1") === false, "1 not valid HOST");
	assert.ok( esHost("_") === false, "_ not valid HOST");
	assert.ok( esHost("-") === false, "- not valid HOST");
	assert.ok( esHost("255.255.255.256") === false, "255.255.255.256 invalid HOST");
	assert.ok( esHost("HOST.1") === false, "HOST.1 invalid HOST");
	assert.ok( esHost("HOSTpuntofinal.") === false, "HOSTpuntofinal. invalid HOST");
	assert.ok( esHost("") === false, "empty invalid HOST");
	assert.ok( esHost() === false, "without parameters invalid HOST");
});